﻿using Microsoft.WindowsAzure.StorageClient.Protocol;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace funFXDistributionReservation
{
    public class Azuretableoperation
    {
        public Task<string> ReservationRetryCountLog(string AzureAccountName, string AzureStorageKey, string AzureTable, RetryMessageCountModel aRetryMessageCountModel)// List<PMSDataCompanyRequest> pMSDataCompanyRequest)
        {

            
            var _result = string.Empty;
            var context = new DynamicTableContext(AzureTable, new Credentials(AzureAccountName, AzureStorageKey));
            _result = context.Insert(aRetryMessageCountModel);
            return Task.FromResult(_result);
        }


        //public  Task<string> UpdatePmsCompanyDataLog(string AzureAccountName, string AzureStorageKey, string AzureTable, RetryMessageCountModel pMSDataCompanyRequest)
        //{
        //    var incommingModelRequest = JsonConvert.SerializeObject(pMSDataCompanyRequest.XmlMessages);
        //    var _result = string.Empty;
        //    var context = new DynamicTableContext(AzureTable, new Credentials(AzureAccountName, AzureStorageKey));
        //    _result = context.InsertOrReplace(pMSDataCompanyRequest);
        //    return  Task.FromResult(_result);
        //}
    }

    public class RetryMessageCountModel
    {
        public string PartitionKey { get; set; }
        public string RowKey { get; set; } 
        public long PmsCode { get; set; }
        public string TokenNo { get; set; }
        public int RetryCount { get; set; }
        public string ErrorMessage { get; set; }
        public string RetryTimeUTC { get; set; }
        public string RetryTimeLocal { get; set; }
    }
}
